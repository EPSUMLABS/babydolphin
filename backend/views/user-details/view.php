<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\api\models\UserDetails */

$this->title ='View User'; //$model->user_id;
$this->params['breadcrumbs'][] = ['label' => 'User Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card" style="width:1050px">
<div class="header">
<div class="user-details-view">
<h1><?= Html::encode($this->title) ?></h1>
</div>
<div class="body">

  <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'user_id',
            'user_name',
            'user_email:email',
            'user_contact',
            //'user_last_login',
            'user_type',
            //'is_blocked',
            //'added_on',
            //'updated_on',
            //'is_delete',
            //'sso_id',
        ],
    ]) ?>
     <p style="float:right">
        <!-- <?php //Html::a('Update', ['update', 'id' => $model->user_id], ['class' => 'btn btn-primary']) ?> -->
        <?= Html::a('Delete', ['delete', 'id' => $model->user_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

</div>
</div>
</div>