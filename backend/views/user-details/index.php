<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserDetailsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Details';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card" style="width:1050px">
<div class="header">


    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
</div>
<div class="body">
<div class="user-details-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'user_id',
            'user_name',
            'user_email:email',
            'user_contact',
            //'user_last_login',
            'user_type',
            //'is_blocked',
            //'added_on',
            //'updated_on',
            //'is_delete',
            //'sso_id',

            ['class' => 'yii\grid\Reviewaction'],
        ],
    ]); ?>
</div>
</div>
</div>