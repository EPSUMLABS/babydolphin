<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\api\models\Review */

$this->title ='View Reviews';
$this->params['breadcrumbs'][] = ['label' => 'Reviews', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card" style="width:1050px">
<div class="header">
<div class="review-view">

    <h1><?= Html::encode($this->title) ?></h1>
</div>
<div class="body">
   

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'review_id',
            'user.user_name',
            'rating',
            'remark',
            'added_on',
            'updated_on',
            //'is_delete',
        ],
    ]) ?>
     <p style="float:right">
        <?= Html::a('Delete', ['delete', 'id' => $model->review_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

</div>
</div>
</div>