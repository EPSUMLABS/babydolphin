<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
<div class="row">
    <div class='col-lg-4'></div>
     <div class="col-lg-4">
     <!--<img src='../../images/boat_logo1.png'>-->
        <div class='card'>
        <div class='header'>
        <center><h1><?= Html::encode($this->title) ?></h1>
        <p>Please fill out the following fields to login:</p> </center>

        </div>
        <div class='body'>
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?php //$form->field($model, 'rememberMe')->checkbox() ?>

                <div class="form-group">
                  <center><?= Html::submitButton('LOGIN', ['class' => 'btn btn-primary', 'name' => 'login-button','style'=>'width:320px;height:35px']) ?>
                </center>
                </div>

            <?php ActiveForm::end(); ?>
            </div>
            </div>
        </div>
    </div>
</div>
