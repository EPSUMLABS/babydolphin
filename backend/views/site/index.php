<?php

/* @var $this yii\web\View */
// namespace backend\modules\api\controllers\AppController;
$this->title = 'BabyDolphin|Admin';
?>
<div class="site-index">
 <h3 style="text-align:center;margin-top:-10px">Welcome Admin</h3>
        <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="info-box bg-red">
                        <div class="icon">
                            <i class="material-icons">bookmark</i>
                        </div>
                        <div class="content">
                            <div class="text">BOOKINGS TODAY</div>
                            <p style="font-size:20px;font-weight:bold" id="booktoday"></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="info-box bg-indigo">
                        <div class="icon">
                            <i class="material-icons">face</i>
                        </div>
                        <div class="content">
                            <div class="text">TOTAL USERS</div>
                            <p style="font-size:20px;font-weight:bold" id="totaluser"></p>
                        </div>
                    </div>
                
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="info-box bg-orange">
                <div class="icon">
                <i class="material-icons">edit</i>
                </div>
                <div class="content">
                 <div class="text" id="statusval"></div>
                 <button type="button" class="btn btn-lg btn-success" name="button" onclick="myFunction()">Change Status</button>
                     </div>
                    </div>
                </div>
                </div>
            </div>
   

        
    

    <div class="body-content">
    <div class="card">
    <div class="body">

    <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                NEWS LETTER
                               
                            </h2>
                           
                        </div>
                        <div class="body">
                            <textarea id="ckeditor">
                                <h2>WYSIWYG Editor</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ullamcorper sapien non nisl facilisis bibendum in quis tellus. Duis in urna bibendum turpis pretium fringilla. Aenean neque velit, porta eget mattis ac, imperdiet quis nisi. Donec non dui et tortor vulputate luctus. Praesent consequat rhoncus velit, ut molestie arcu venenatis sodales.</p>
                                <h3>Lacinia</h3>
                                <ul>
                                    <li>Suspendisse tincidunt urna ut velit ullamcorper fermentum.</li>
                                    <li>Nullam mattis sodales lacus, in gravida sem auctor at.</li>
                                    <li>Praesent non lacinia mi.</li>
                                    <li>Mauris a ante neque.</li>
                                    <li>Aenean ut magna lobortis nunc feugiat sagittis.</li>
                                </ul>
                                <h3>Pellentesque adipiscing</h3>
                                <p>Maecenas quis ante ante. Nunc adipiscing rhoncus rutrum. Pellentesque adipiscing urna mi, ut tempus lacus ultrices ac. Pellentesque sodales, libero et mollis interdum, dui odio vestibulum dolor, eu pellentesque nisl nibh quis nunc. Sed porttitor leo adipiscing venenatis vehicula. Aenean quis viverra enim. Praesent porttitor ut ipsum id ornare.</p>
                            </textarea>
                        </div>
                    </div>
                </div>
            </div>
             
</div>
