<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\SelectAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

SelectAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    
    <?php $this->head() ?>
</head>
<body style="background-color:#fff" >
<?php $this->beginBody() ?>
<nav class="navbar collapse navbar-collapse" style="background-image: linear-gradient(to bottom, #c90035, #cf1c2c, #d42e22, #d83e16, #da4c00);">
        <div class="container-fluid">
        <div class="navbar-header">
                <?= Html::a('BABY DOLPHIN', ['/site/index'],['class'=>'navbar-brand','style' => 'font-weight:bold;color:#fff']) ?></span>
            </div>
            <div class="nav navbar-nav navbar-right">
            <?= Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'LOGOUT (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout','style'=>'font-weight:bold;color:#fff;padding-top:24px']
            )
             . Html::endForm()?>
            </div>
            </nav>
 <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                <img src= '../../images/boat_logo1.png' style="height:100px;width:250px"/>
                </div>
                
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    <li class="active">
                   
                            <span style="display:flex;padding-top:10px"><i class="material-icons" style="margin-left:15px">home</i>
                            <?= Html::a('Home', ['/site/index'],['style' => 'font-weight:bold;color:#333333']) ?></span>
                        
                    </li>
                    <li>
                       
                    <span style="display:flex;padding-top:10px"><i class="material-icons" style="margin-left:15px">bookmark</i>
                            <?= Html::a('Bookings', ['/booking/index'],['style' => 'font-weight:bold;color:#333333']) ?></span>
                    </li>
                    <li>
                        
                    <span style="display:flex;padding-top:10px"><i class="material-icons" style="margin-left:15px">list</i>
                    <?= Html::a('Boat Categories', ['/boat-category/index'],['style' => 'font-weight:bold;color:#333333']) ?></span>
                       
                    </li>
                    <li>
                        
                    <span style="display:flex;padding-top:10px"><i class="material-icons" style="margin-left:15px">face</i>
                    <?= Html::a('Users', ['/user-details/index'],['style' => 'font-weight:bold;color:#333333']) ?></span>
                       
                    </li>
                    <li>
                       
                    <span style="display:flex;padding-top:10px"><i class="material-icons" style="margin-left:15px">rate_review</i>
                            <?= Html::a('Reviews', ['/review/index'],['style' => 'font-weight:bold;color:#333333']) ?></span>
                        
                    </li>
                    <li>
                        
                    <span style="display:flex;padding-top:10px"><i class="material-icons" style="margin-left:15px">image</i>
                            <?= Html::a('Banners', ['/banners/index'],['style' => 'font-weight:bold;color:#333333']) ?></span>
                        </a>
                    </li>
                    <li>
                        
                    <!--<span style="display:flex;padding-top:10px"><i class="material-icons" style="margin-left:15px">image</i>-->
                           <?php
            //                 Html::beginForm(['/site/logout'], 'post')
            // . Html::submitButton(
            //     'LOGOUT (' . Yii::$app->user->identity->username . ')',
            //     ['class' => 'btn btn-link logout','style'=>'font-weight:bold;color:#333333']
            // )
            //  . Html::endForm()
             ?>
                        
                    </li>
                   
                </ul>
            </div>
        </aside>
<section class="content">
<div class="container-fluid">

    <?php
    // NavBar::begin();
    // // $menuItems = [
    // //     ['label' => 'Home', 'url' => ['/site/index']],
    // //     ['label'=>'Bookings','url'=>['/booking/index']],
    // //     ['label'=>'Boat Categories','url'=>['/boat-category/index']],
    // //     ['label'=>'Users','url'=>['/user-details/index']],
    // //     ['label'=>'Reviews','url'=>['/review/index']],
    // //     ['label'=>'Banners','url'=>['/banners/index']],
        
    // // ];
    // if (Yii::$app->user->isGuest) {
    //     $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    // } else {
    //     $menuItems[] = '<li>'
    //         . Html::beginForm(['/site/logout'], 'post')
    //         . Html::submitButton(
    //             'LOGOUT (' . Yii::$app->user->identity->username . ')',
    //             ['class' => 'btn btn-link logout','style'=>'padding-top:24px;color:white']
    //         )
            
    //         . Html::endForm()
    //         . '</li>';
    // }
    // echo Nav::widget([
    //     'options' => ['class' => 'navbar-nav navbar-right'],
    //     'items' => $menuItems,
    // ]);
    // NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>
</section>



<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
