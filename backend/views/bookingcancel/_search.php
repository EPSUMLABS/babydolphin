<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\BookingcancelSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="booking-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'booking_id') ?>

    <?= $form->field($model, 'book_id') ?>

    <?= $form->field($model, 'boat_id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'number_of_person') ?>

    <?php // echo $form->field($model, 'total_billing_amount') ?>

    <?php // echo $form->field($model, 'mode_of_pay') ?>

    <?php // echo $form->field($model, 'pay_status') ?>

    <?php // echo $form->field($model, 'date_of_booking') ?>

    <?php // echo $form->field($model, 'book_status') ?>

    <?php // echo $form->field($model, 'updated_on') ?>

    <?php // echo $form->field($model, 'offer_applied') ?>

    <?php // echo $form->field($model, 'book_via') ?>

    <?php // echo $form->field($model, 'date_of_ride') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
