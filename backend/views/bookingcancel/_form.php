<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\api\models\Booking */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="booking-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'book_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'boat_id')->textInput() ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'number_of_person')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'total_billing_amount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mode_of_pay')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pay_status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'date_of_booking')->textInput() ?>

    <?= $form->field($model, 'book_status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'updated_on')->textInput() ?>

    <?= $form->field($model, 'offer_applied')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'book_via')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'date_of_ride')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
