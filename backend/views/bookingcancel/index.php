<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\BookingcancelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Canceled Bookings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card" style="width:1050px">
<div class="header">
<div class="booking-index">

  <h1><?= Html::encode($this->title) ?></h1>
<hr>
</div>
<div class="body">


  
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

   

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
            'book_id',
            [
                'attribute'=>'boat_id',
                'value'=>'boat.category_name'
            ],
            [
                'attribute'=>'user_id',
                'value'=>'user.user_name',
            ],
            'number_of_person',
            'total_billing_amount',
            //'mode_of_pay',
            //'pay_status',
            //'date_of_booking',
            'book_status',
            //'updated_on',
            //'is_delete',
            //'offer_applied',
            //'book_via',
            'date_of_ride',

            ['class' => 'yii\grid\Bookingaction'],
        ],
    ]); ?>
    </div>
    </div>
</div>
