<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\api\models\Booking */

$this->title = 'View Booking';
$this->params['breadcrumbs'][] = ['label' => 'Bookings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card" style="width:1050px">
<div class="header">
<h1><?= Html::encode($this->title) ?></h1>
</div>
<div class="body">
<div class="booking-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'book_id',
            'boat.category_name',
            'user.user_name',
            'number_of_person',
            'total_billing_amount',
            'mode_of_pay',
            'pay_status',
            'date_of_booking',
            'book_status',
            //'offer_applied',
            'book_via',
            'date_of_ride',
        ],
    ]) ?>
</div>
</div>
</div>