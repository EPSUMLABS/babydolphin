<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\modules\api\models\BoatCategory;

/* @var $this yii\web\View */
/* @var $model backend\modules\api\models\Booking */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="booking-form">
<div class="col-lg-6">
    <?php $form = ActiveForm::begin(); ?>

    <?php
     $oidhcar='BD';
     $chars = "0123456789";
     $random=substr(str_shuffle($chars),0,10);
     $Bookid=$oidhcar.$random;
     ?>
     <?=$form->field($model,'book_id',['inputOptions' => ['value' => $Bookid]])->hiddenInput()->label(false) ?>

     <?= $form->field($model, 'boat_id')->dropDownList(
        ArrayHelper::map(BoatCategory::find()->all(),'boat_id','category_name'),
        ['prompt'=>'Select Boat']
    ) ?>

    <?= $form->field($model, 'user_id',['inputOptions' => ['value' => 4]])->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'number_of_person')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'total_billing_amount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mode_of_pay',['inputOptions' => ['value' => 'cod']])->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'pay_status')->textInput(['maxlength' => true]) ?>

    <?php //$form->field($model, 'date_of_booking')->textInput() ?>

    <?= $form->field($model, 'book_status',['inputOptions' => ['value' => 'active']])->hiddenInput()->label(false)?>

    <?php //$form->field($model, 'updated_on')->textInput() ?>

    <?php //$form->field($model, 'is_delete')->textInput(['value'=>0])->hiddenInput()->label(false) ?>

    <?php //$form->field($model, 'offer_applied')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'book_via',['inputOptions' => ['value' => 'web']])->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'date_of_ride')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
</div>
    <?php ActiveForm::end(); ?>

</div>
