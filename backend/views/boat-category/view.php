<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\api\models\BoatCategory */

$this->title = 'View Boats';
$this->params['breadcrumbs'][] = ['label' => 'Boat Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card" style="width:1050px">

<div class="header">
<div class="boat-category-view">
    <h1><?= Html::encode($this->title) ?></h1>
   
</div>
<div class="body">
   

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'boat_id',
            'category_name',
            'boat_description',
            'minimum_fare',
            'capacity',
            'min_fare_applicable',
            'cost_add_on_person',
            'added_on',
            'updated_on',
            //'is_delete',
            'number_of_boats',
        ],
    ]),
 
  
     Html::a('Delete', ['delete', 'id' => $model->boat_id], [
        'class' => 'btn btn-danger',
        'style'=>'float:right',
        'data' => [
            'confirm' => 'Are you sure you want to delete this item?',
            'method' => 'post',
        ],
    ]) ,
    Html::a('Update', ['update', 'id' => $model->boat_id], ['class' => 'btn btn-primary','style'=>'margin-right:5px;float:right'])

    ?>
 

</div>
</div>
</div>