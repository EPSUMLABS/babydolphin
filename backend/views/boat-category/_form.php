<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\api\models\BoatCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="boat-category-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-lg-6">
    <?= $form->field($model, 'category_name')->textInput(['maxlength' => true]) ?>

     <?= $form->field($model, 'boat_description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'minimum_fare')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'capacity')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'min_fare_applicable')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cost_add_on_person')->textInput(['maxlength' => true]) ?>

    <?php // $form->field($model, 'added_on')->textInput() ?>

    <?php //$form->field($model, 'updated_on')->textInput() ?>

    <?php //$form->field($model, 'is_delete')->textInput() ?>

    <?= $form->field($model, 'number_of_boats')->textInput(['maxlength' => true]) ?>
   
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success','style'=>'width:100px']) ?>
    </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
