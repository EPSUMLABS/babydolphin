<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\api\models\BoatCategory */

$this->title = 'Update Boat Category'; //. $model->boat_id;
$this->params['breadcrumbs'][] = ['label' => 'Boat Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->boat_id, 'url' => ['view', 'id' => $model->boat_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="boat-category-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model
    ]) ?>

</div>
