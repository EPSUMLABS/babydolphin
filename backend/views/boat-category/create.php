<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\api\models\BoatCategory */

$this->title = 'Create Boat Category';
$this->params['breadcrumbs'][] = ['label' => 'Boat Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="boat-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
