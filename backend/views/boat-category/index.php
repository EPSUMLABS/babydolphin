<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\BoatCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Boat Categories';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card" style="width:1050px">
<div class="header">
<div class="boat-category-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <hr>
    
</div>
<div class="body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            

            'boat_id',
            'category_name',
            'boat_description',
            'minimum_fare',
            'capacity',
            'min_fare_applicable',
            //'cost_add_on_person',
            //'added_on',
            //'updated_on',
            //'is_delete',
            //'number_of_boats',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <p style="float:right">
        <?= Html::a('Create Boat Category', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
   
</div>
</div>
</div>