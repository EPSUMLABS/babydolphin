<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\BoatCategorySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="boat-category-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'boat_id') ?>

    <?= $form->field($model, 'category_name') ?>

    <?= $form->field($model, 'boat_description') ?>

    <?= $form->field($model, 'minimum_fare') ?>

    <?= $form->field($model, 'capacity') ?>

    <?= $form->field($model, 'min_fare_applicable') ?>

    <?php // echo $form->field($model, 'cost_add_on_person') ?>

    <?php // echo $form->field($model, 'added_on') ?>

    <?php // echo $form->field($model, 'updated_on') ?>

    <?php // echo $form->field($model, 'is_delete') ?>

    <?php // echo $form->field($model, 'number_of_boats') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
