<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\BannersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Banners';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card" style="width:1050px">
<div class="header">
<div class="banners-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <hr>
    </div>
    <div class="body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
 
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'banner_id',
            'banner_path',
            'updated_on',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <p style="float:right">
        <?= Html::a('Create Banners', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

</div>
</div>
</div>
