<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\api\models\Banners */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="banners-form">
<div class="col-lg-6">
    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>

    <?php //$form->field($model, 'banner_path')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'banner_name')->textInput(['maxlength' => true]) ?>

    <?=$form->field($model, 'file')->fileInput()?>

    <?php //$form->field($model, 'updated_on')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
</div>
    <?php ActiveForm::end(); ?>

</div>
