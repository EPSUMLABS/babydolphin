<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\api\models\Banners */

$this->title = 'View Banners';
$this->params['breadcrumbs'][] = ['label' => 'Banners', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card" style="width:1050px">
<div class="header">
<div class="banners-view">

    <h1><?= Html::encode($this->title) ?></h1>
</div>
<div class="body">
    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'banner_id',
            'banner_path',
            'updated_on',
        ],
    ]) ?>
    <p style="float:right">
        <?= Html::a('Update', ['update', 'id' => $model->banner_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->banner_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

</div>
</div>
