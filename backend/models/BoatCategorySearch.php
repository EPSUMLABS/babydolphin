<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\api\models\BoatCategory;

/**
 * BoatCategorySearch represents the model behind the search form of `backend\modules\api\models\BoatCategory`.
 */
class BoatCategorySearch extends BoatCategory
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['boat_id', 'is_delete'], 'integer'],
            [['category_name','boat_description', 'minimum_fare', 'capacity', 'min_fare_applicable', 'cost_add_on_person', 'added_on', 'updated_on', 'number_of_boats'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BoatCategory::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'boat_id' => $this->boat_id,
            'added_on' => $this->added_on,
            'updated_on' => $this->updated_on,
            'is_delete' => $this->is_delete,
        ]);

        $query->andFilterWhere(['like', 'category_name', $this->category_name])
            ->andFilterWhere(['like', 'minimum_fare', $this->minimum_fare])
            ->andFilterWhere(['like', 'capacity', $this->capacity])
            ->andFilterWhere(['like', 'min_fare_applicable', $this->min_fare_applicable])
            ->andFilterWhere(['like', 'boat_description', $this->boat_description])
            ->andFilterWhere(['like', 'cost_add_on_person', $this->cost_add_on_person])
            ->andFilterWhere(['like', 'number_of_boats', $this->number_of_boats]);

        return $dataProvider;
    }
}
