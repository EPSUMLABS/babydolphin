<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\api\models\Review;

/**
 * ReviewSearch represents the model behind the search form of `backend\modules\api\models\Review`.
 */
class ReviewSearch extends Review
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['review_id', 'is_delete'], 'integer'],
            [['rating','user_id','remark', 'added_on', 'updated_on'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Review::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('user');

        // grid filtering conditions
        $query->andFilterWhere([
            'review_id' => $this->review_id,
            'added_on' => $this->added_on,
            'updated_on' => $this->updated_on,
            'is_delete' => $this->is_delete,
        ]);

        $query->andFilterWhere(['like', 'rating', $this->rating])
            ->andFilterWhere(['like', 'remark', $this->remark])
            ->andFilterWhere(['like', 'user_details.user_name', $this->user_id]);

        return $dataProvider;
    }
}
