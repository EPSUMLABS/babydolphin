<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\api\models\Booking;

/**
 * BookingSearch represents the model behind the search form of `backend\modules\api\models\Booking`.
 */
class BookingSearch extends Booking
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['booking_id','is_delete'], 'integer'],
            [['number_of_person','user_id','book_id','boat_id', 'total_billing_amount', 'mode_of_pay', 'pay_status', 'date_of_booking', 'book_status', 'updated_on', 'offer_applied', 'book_via', 'date_of_ride'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Booking::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('user');
        $query->joinWith('boat');

        

        // grid filtering conditions
        $query->andFilterWhere([
            'booking_id' => $this->booking_id,
            'date_of_booking' => $this->date_of_booking,
            'updated_on' => $this->updated_on,
            'is_delete' => $this->is_delete,
        ]);

        $query->andFilterWhere(['like', 'number_of_person', $this->number_of_person])
            ->andFilterWhere(['like', 'total_billing_amount', $this->total_billing_amount])
            ->andFilterWhere(['like', 'book_id', $this->book_id])
            ->andFilterWhere(['like', 'mode_of_pay', $this->mode_of_pay])
            ->andFilterWhere(['like', 'pay_status', $this->pay_status])
            ->andFilterWhere(['like', 'book_status', $this->book_status])
            ->andFilterWhere(['like', 'offer_applied', $this->offer_applied])
            ->andFilterWhere(['like', 'book_via', $this->book_via])
            ->andFilterWhere(['like', 'date_of_ride', $this->date_of_ride])
            ->andFilterWhere(['like', 'boat_category.category_name', $this->boat_id])
            ->andFilterWhere(['like', 'user_details.user_name', $this->user_id]);
        return $dataProvider;
    }
}
