<?php

namespace backend\modules\api\models;

use Yii;

/**
 * This is the model class for table "booking_rule".
 *
 * @property int $id
 * @property string $allowed_upto
 * @property int $booking_allowed
 * @property string $is_update
 * @property int $is_delete
 */
class BookingRule extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'booking_rule';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['allowed_upto', 'booking_allowed', 'is_delete'], 'required'],
            [['allowed_upto', 'is_update'], 'safe'],
            [['booking_allowed', 'is_delete'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'allowed_upto' => 'Allowed Upto',
            'booking_allowed' => 'Booking Allowed',
            'is_update' => 'Is Update',
            'is_delete' => 'Is Delete',
        ];
    }
}
