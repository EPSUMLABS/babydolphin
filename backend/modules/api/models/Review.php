<?php

namespace backend\modules\api\models;

use Yii;

/**
 * This is the model class for table "review".
 *
 * @property int $review_id
 * @property int $user_id
 * @property string $rating
 * @property string $remark
 * @property string $added_on
 * @property string $updated_on
 * @property int $is_delete
 *
 * @property UserDetails $user
 */
class Review extends \yii\db\ActiveRecord
{
    const SCENARIO_CREATE='create';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'review';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'rating', 'remark', 'added_on', 'is_delete'], 'required'],
            [['user_id', 'is_delete'], 'integer'],
            [['added_on', 'updated_on'], 'safe'],
            [['rating'], 'string', 'max' => 50],
            [['remark'], 'string', 'max' => 100],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['user_id' => 'user_id']],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = ['user_id','rating','remark'];//scenarios value only accepted
        return $scenarios;
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'review_id' => 'Review Id',
            'user_id' => 'User Name',
            'rating' => 'Rating',
            'remark' => 'Remark',
            'added_on' => 'Added On',
            'updated_on' => 'Updated On',
            'is_delete' => 'Is Delete',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['user_id' => 'user_id']);
    }
}
