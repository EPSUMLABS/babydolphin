<?php

namespace backend\modules\api\models;

use Yii;

/**
 * This is the model class for table "boat_category".
 *
 * @property int $boat_id
 * @property string $category_name
 * @property string $boat_description
 * @property string $minimum_fare
 * @property string $capacity
 * @property string $min_fare_applicable
 * @property string $cost_add_on_person
 * @property string $added_on
 * @property string $updated_on
 * @property int $is_delete
 * @property string $number_of_boats
 *
 * @property Booking[] $bookings
 */
class BoatCategory extends \yii\db\ActiveRecord
{
    const SCENARIO_CREATE='create';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'boat_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_name', 'boat_description', 'minimum_fare', 'capacity', 'min_fare_applicable', 'cost_add_on_person', 'added_on', 'is_delete', 'number_of_boats'], 'required'],
            [['added_on', 'updated_on'], 'safe'],
            [['is_delete'], 'integer'],
            [['category_name', 'minimum_fare', 'cost_add_on_person'], 'string', 'max' => 100],
            [['boat_description'], 'string', 'max' => 500],
            [['capacity', 'min_fare_applicable', 'number_of_boats'], 'string', 'max' => 50],
            [['category_name'], 'unique'],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = ['category_name','minimum_fare','capacity','min_fare_applicable','cost_add_on_person','number_of_boats'];//scenarios value only accepted
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'boat_id' => 'Boat ID',
            'category_name' => 'Category Name',
            'boat_description' => 'Boat Description',
            'minimum_fare' => 'Minimum Fare',
            'capacity' => 'Capacity',
            'min_fare_applicable' => 'Min Fare Applicable',
            'cost_add_on_person' => 'Cost Add On Person',
            'added_on' => 'Added On',
            'updated_on' => 'Updated On',
            'is_delete' => 'Is Delete',
            'number_of_boats' => 'Number Of Boats',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookings()
    {
        return $this->hasMany(Booking::className(), ['boat_id' => 'boat_id']);
    }
}
