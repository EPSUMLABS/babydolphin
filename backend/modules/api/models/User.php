<?php

namespace backend\modules\api\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $user_contact_no
 * @property string $user_type
 * @property int $status
 * @property string $email
 * @property string $created_at
 * @property string $updated_at
 * @property string $is_blocked
 *
 * @property Booking[] $bookings
 * @property Review[] $reviews
 */
class User extends \yii\db\ActiveRecord
{
    const SCENARIO_CREATE='create';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'password_hash', 'user_contact_no', 'user_type', 'status', 'email', 'is_blocked'], 'required'],
            [['user_contact_no', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['username', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['user_type', 'is_blocked'], 'string', 'max' => 100],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }
     
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = ['username','password_hash','user_contact_no','email'];//scenarios value only accepted
        return $scenarios;
    }



    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'user_contact_no' => 'User Contact No',
            'user_type' => 'User Type',
            'status' => 'Status',
            'email' => 'Email',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'is_blocked' => 'Is Blocked',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookings()
    {
        return $this->hasMany(Booking::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews()
    {
        return $this->hasMany(Review::className(), ['user_id' => 'id']);
    }
}
