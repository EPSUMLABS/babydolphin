<?php

namespace backend\modules\api\models;

use Yii;

/**
 * This is the model class for table "user_details".
 *
 * @property int $user_id
 * @property string $user_name
 * @property string $user_email
 * @property string $user_contact
 * @property string $user_password
 * @property string $user_last_login
 * @property string $user_type
 * @property int $is_blocked
 * @property string $added_on
 * @property string $updated_on
 * @property int $is_delete
 * @property string $sso_id
 *
 * @property Booking[] $bookings
 * @property Review[] $reviews
 */
class UserDetails extends \yii\db\ActiveRecord
{
    const SCENARIO_CREATE='create';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_name', 'user_email', 'user_contact', 'user_password', 'user_last_login', 'user_type', 'is_blocked', 'is_delete'], 'required'],
            [['user_last_login', 'added_on', 'updated_on'], 'safe'],
            [['is_blocked', 'is_delete'], 'integer'],
            [['user_name', 'user_email', 'user_contact', 'user_password', 'user_type', 'sso_id'], 'string', 'max' => 100],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = ['user_name','user_email','user_contact','user_password'];//scenarios value only accepted
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'user_name' => 'User Name',
            'user_email' => 'User Email',
            'user_contact' => 'User Contact',
            'user_password' => 'User Password',
            'user_last_login' => 'User Last Login',
            'user_type' => 'User Type',
            'is_blocked' => 'Is Blocked',
            'added_on' => 'Added On',
            'updated_on' => 'Updated On',
            'is_delete' => 'Is Delete',
            'sso_id' => 'Sso ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookings()
    {
        return $this->hasMany(Booking::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews()
    {
        return $this->hasMany(Review::className(), ['user_id' => 'user_id']);
    }
}
