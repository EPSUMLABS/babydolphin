<?php

namespace backend\modules\api\models;

use Yii;

/**
 * This is the model class for table "banners".
 *
 * @property int $banner_id
 * @property string $banner_path
 * @property string $updated_on
 */
class Banners extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $file;
    public static function tableName()
    {
        return 'banners';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['banner_path'], 'required'],
            [['updated_on'], 'safe'],
            [['file'],'file'],
            [['banner_path'], 'string', 'max' => 200],
            [['banner_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'banner_id' => 'Banner ID',
            'banner_name'=>'Banner Name',
            'banner_path' => 'Banner Path',
            'file'=>'Banner',
            'updated_on' => 'Updated On',
        ];
    }
}
