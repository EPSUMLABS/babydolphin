<?php

namespace backend\modules\api\models;

use Yii;

/**
 * This is the model class for table "booking".
 *
 * @property int $booking_id
 * @property string $book_id
 * @property int $boat_id
 * @property int $user_id
 * @property string $number_of_person
 * @property string $total_billing_amount
 * @property string $mode_of_pay
 * @property string $pay_status
 * @property string $date_of_booking
 * @property string $book_status
 * @property string $updated_on
 * @property int $is_delete
 * @property string $offer_applied
 * @property string $book_via
 * @property string $date_of_ride
 *
 * @property BoatCategory $boat
 * @property UserDetails $user
 * @property Person[] $people
 */
class Booking extends \yii\db\ActiveRecord
{
    const SCENARIO_CREATE='create';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'booking';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['boat_id', 'user_id', 'number_of_person', 'total_billing_amount', 'date_of_ride'], 'required'],
            [['boat_id', 'user_id'], 'integer'],
            [['date_of_booking', 'updated_on','book_id','offer_applied', ], 'safe'],
            [['book_id', 'mode_of_pay', 'pay_status', 'book_status', 'offer_applied','mode_of_pay','pay_status', 'book_via', 'book_via', 'date_of_ride'], 'string', 'max' => 100],
            [['number_of_person', 'total_billing_amount'], 'string', 'max' => 50],
            [['boat_id'], 'exist', 'skipOnError' => true, 'targetClass' => BoatCategory::className(), 'targetAttribute' => ['boat_id' => 'boat_id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['user_id' => 'user_id']],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = ['boat_id','user_id','number_of_person','total_billing_amount','mode_of_pay','pay_status','book_via'];//scenarios value only accepted
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'booking_id' => 'Booking ID',
            'book_id' => 'Book ID',
            'boat_id' => 'Boat ID',
            'user_id' => 'User ID',
            'number_of_person' => 'Number Of Person',
            'total_billing_amount' => 'Total Billing Amount',
            'mode_of_pay' => 'Mode Of Pay',
            'pay_status' => 'Pay Status',
            'date_of_booking' => 'Date Of Booking',
            'book_status' => 'Book Status',
            'updated_on' => 'Updated On',
            //'is_delete' => 'Is Delete',
            'offer_applied' => 'Offer Applied',
            'book_via' => 'Book Via',
            'date_of_ride' => 'Date Of Ride',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoat()
    {
        return $this->hasOne(BoatCategory::className(), ['boat_id' => 'boat_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeople()
    {
        return $this->hasMany(Person::className(), ['booking_id' => 'booking_id']);
    }
}
