<?php

namespace backend\modules\api\controllers;
use yii\web\Controller;
use Yii;
use backend\modules\api\models\User;
use backend\modules\api\models\UserDetails;
use backend\modules\api\models\BoatCategory;
use backend\modules\api\models\Booking;
use backend\modules\api\models\Review;
use backend\modules\api\models\Banners;
use backend\modules\api\models\BookingRule;


class AppController extends Controller
{
    public $enableCsrfValidation=false;
    public function actionIndex()
    {

        echo 'this is test';exit;
        return $this->render('index');
    }

    //Add User API...............
    public function actionAdduser()
    {
     \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;//this will return response as json
     $UserDetails = new UserDetails();
     $UserDetails->scenario = UserDetails::SCENARIO_CREATE;
     $UserDetails->attributes= \Yii::$app->request->post();
        if($UserDetails->validate()){
            $UserDetails->save();
            return array('status' => 'success','message'=>'User Added','data'=>$UserDetails);
        }
        else{
            return array('status'=>'failed','data'=>$UserDetails->getErrors());
        }
    //  return array('status'=>200 , 'message'=>'success');
    }

    //View User Api.............
    public function actionViewuser()
    {
        \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;//this will return response as json
        //$user->attributes= \Yii::$app->request->get();

        $UserDetails= UserDetails::find()->all();

        if(count($UserDetails)>0){
            return array('status'=> 'success', 'data'=>count($UserDetails));
        }
        else
        {
            return array('status'=>'failed','data'=>'no records found');
        }

    }

    //Update User Api..............
    public function actionUpdateuser(){
        \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;//this will return response as json
        $attributes= \Yii::$app->request->post();
        $UserDetails = UserDetails::find()->where(['user_id' => $attributes['user_id'] ])->one();
        if($UserDetails)
        {
            
            $UserDetails->attributes= \Yii::$app->request->post();
            
            $UserDetails->save();
            return array('status'=> 'success', 'data'=>'user updated');
           
        }
        elseif($UserDetails==NULL){
            return array('status'=>'failed','data'=>'no records found');
        }
        else{
            return array('status'=>'failed','data'=>'some error occured');
        }
     


    }

    //User Login Api..........
    public function actionUserlogin(){
        \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;//this will return response as json
        $attributes= \Yii::$app->request->post();
        //$UserDetails = UserDetails::find()->where(['user_email' => $attributes['user_email'] ] && ['user_password'=>$attributes['user_password']])->one();
        $UserDetails = UserDetails::find()->where(['user_email' => $attributes['user_email'] , 'user_password'=> $attributes['user_password'] ])->one();
        if($UserDetails)
        {
            return array('data'=>$UserDetails,'status'=> 'success','message'=>'Login Successful');
            // $session = Yii::$app->session;
            // $session->set('userid',$UserDetails['user_id']);
        }
        else{
            return array('status'=>'failed','message'=>'invalid user id or password');
        }
    
    }

    //forgot password ........
    public function actionUserforgotpassword(){
        \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;//this will return response as json
        $attributes= \Yii::$app->request->post();
        $UserDetails = UserDetails::find()->where(['user_email' => $attributes['user_email']])->one();
        if($UserDetails)
        {
            $password= UserDetails::find()->select(['user_password','user_email'])->where(['user_email'=> $attributes['user_email']])->one();
            $user_email=$password['user_email'];
            $user_password=$password['user_password'];
            $value= \Yii::$app->mailer->compose()
            ->setFrom(['kajorib9@gmail.com'])
            ->setTo($user_email)
            ->setSubject('Your password for babydolphin')
            ->setHtmlBody('Your password is'.$user_password)
            ->send();   
            return array('status'=>'success','message'=>'Email has been sent','password'=>$user_password,'user'=>$user_email);
        }
        else{
            return array('status'=>'failed','message'=>'invalid email id');
        }

    }
    
    //User Change Password..........
    public function actionUserchangepassword(){
        \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;//this will return response as json
        $attributes= \Yii::$app->request->post();
        $UserDetails = UserDetails::find()->where(['user_id' => $attributes['user_id'] ])->one();
        if($UserDetails){
            $UserDetails->user_password = $attributes['user_password'];
            $UserDetails->save();
            return array('status'=> 'success','message'=>'Password Changed');
            }
            else{
                return array('status'=>'failed','message'=>'invalid user id');
            }


    }
    //Booking Rule display status
    public function actionBookingdisplay(){
        \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;//this will return response as json
        //$user->attributes= \Yii::$app->request->get();

        $bookrule= BookingRule::find()->one();

        if($bookrule){
            return array('status'=> 'success', 'data'=>$bookrule);
        }
        else
        {
            return array('status'=>'failed','data'=>'no records found');
        }

    }
    //Booking Rule
    public function actionBookingrule(){
        \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;//this will return response as json
        $attributes= \Yii::$app->request->post();
        $bookrule=BookingRule::find()->where(['id' => '1' ])->one();
        if($bookrule){
            if($bookrule->booking_allowed==0)
            {
        $bookrule->booking_allowed='1';
        $bookrule->save();
        return array('status'=> 'success','message'=>'Booking rule changed','data'=>$bookrule);
        }
        else{
            $bookrule->booking_allowed='0';
            $bookrule->save();
            return array('status'=> 'success','message'=>'Booking rule changed','data'=>$bookrule);
        }
    }
        else{
            return array('status'=>'failed','message'=>'booking rule not changed');
        }
    }

    
    

    //Block User Api......
    public function actionDeleteuser(){
        \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;//this will return response as json
        $attributes= \Yii::$app->request->post();
        $UserDetails = UserDetails::find()->where(['user_id' => $attributes['user_id'] ])->one();
        if($UserDetails)
        {
            $UserDetails->delete();
            return array('status'=>'success', 'data'=>'Record Deleted');
        }
        else
        {
            return array('status'=>'failed','data'=>'no records found');
        }


    }


    //Add Boat Category
    public function actionAddboat()
    {
        \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;//this will return response as json
        $Boatcategory=new BoatCategory();
        $Boatcategory->scenario = BoatCategory::SCENARIO_CREATE;
        $Boatcategory->attributes= \Yii::$app->request->post();
        if($Boatcategory->validate()){
            $Boatcategory->save();
            return array('status' => 'success','message'=>'Boat Category Added');
        }
        else{
            return array('status'=>'failed','data'=>$Boatcategory->getErrors());
            }


    }

    //View Boat Category
    public function actionViewboat(){
        \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;//this will return response as json
        //$user->attributes= \Yii::$app->request->get();

        $Boatcategory= BoatCategory::find()->all();

        if(count($Boatcategory)>0){
            return array('status'=> 'success', 'data'=>$Boatcategory);
        }
        else
        {
            return array('status'=>'failed','data'=>'no records found');
        }
    }



    //Booking Boat .......
    public function actionBooking(){
        \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;//this will return response as json
        $attributes= \Yii::$app->request->post();
        $session = Yii::$app->session;
        $user=$session->get('userid');
        $oidhcar='BABYDOLPHIN';
        $chars = "0123456789";
        $random=substr(str_shuffle($chars),0,10);
        $Bookid=$oidhcar.$random;
        $Booking= new Booking();
        $Booking->boat_id = $attributes['boat_id'];
        $Booking->book_id = $Bookid;
        $Booking->user_id = $attributes['user_id'];
        $Booking->number_of_person = $attributes['number_of_person'];
        $Booking->total_billing_amount = $attributes['total_billing_amount'];
        $Booking->mode_of_pay = $attributes['mode_of_pay'];
        $Booking->pay_status = $attributes['pay_status'];
        $Booking->book_via = $attributes['book_via'];
        $Booking->date_of_ride = $attributes['date_of_ride'];

        
        $Booking->scenario= Booking::SCENARIO_CREATE;
        //$Booking->attributes= \Yii::$app->request->post();
        if($Booking->validate()){
            $Booking->save();
           
           // echo $Bookid;
            return array('status'=>'success', 'message'=>'Booking Done','data'=>$Booking);
        }
        else{
            return array('status'=>'failed','data'=>$Booking->getErrors());
        }



    }

    //Update Booking Boat
    public function actionUpdatebooking(){
        \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;//this will return response as json
        $attributes= \Yii::$app->request->post();
        $Booking = Booking::find()->where(['booking_id' => $attributes['booking_id'] ])->one();
        if($Booking)
        {
            
            $Booking->attributes= \Yii::$app->request->post();
            // if($Booking->validate()){
            $Booking->save();
            return array('status'=> 'success', 'data'=>'Booking details updated');
            // }
            // else{
            //     return array('status'=>'false','data'=>$Booking->getErrors());
            // }
        }
        elseif($Booking==NULL){
            return array('status'=>'failed','data'=>'no records found');
        }
        else{
            return array('status'=>'failed','data'=>'some error occured');
        }
     
    }

    //Booking on available boats
    public function actionAvailablebooking(){
        \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;//this will return response as json
        $attributes= \Yii::$app->request->post();
        $Booking = Booking::find()->select(['booking_id'])->where(['date_of_ride' => $attributes['date_of_ride'],'book_status'=>'active' ])->all();
        $Boatcategory= BoatCategory::find()->select(['number_of_boats'])->where(['category_name'=> $attributes['category_name']])->one();
        $Bookingrule=BookingRule::find()->select(['booking_allowed'])->where(['id'=>'1'])->one();
        $book_allowed=$Bookingrule['booking_allowed'];
        if($book_allowed==0){
            return array('status'=> 'failed', 'message'=>'Booking is not going on');
        }
        else{
        $Count=count($Booking);
        $Total_boats=$Boatcategory['number_of_boats'];
         //return array('status'=> 'success', 'data'=>$Count, 'boats'=>$Total_boats);
            if($Count<$Total_boats){
                $availableboats=$Total_boats-$Count;
                if($availableboats>0)
                {
                    date_default_timezone_set('Asia/Kolkata');
                    $date= date('d-m-Y');
                    $date2=$attributes['date_of_ride'];
                    $time1=date("H:i:s");
                    $time2='12:00:00';
                    if($date==$date2 && $time2<$time1)
                    {
                  return array('status'=> 'failed', 'message'=>'Booking is available upto 12:00pm');
                }
                else if($date==$date2 && $time2>$time1 ){
                    return array('status'=> 'success', 'message'=>'Booking available','availableboats'=>$availableboats);
                }
             else if($date<$date2){
            return array('status'=> 'success','message'=>'Booking available','availableboats'=>$availableboats);
                }
                else if($date>$date2){
                    return array('status'=> 'failed', 'message'=>'choosing previous date is not allowed');
                }
             }
            
       
    }
}}

//Booking Today Api
public function actionTodaybooking(){
    \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;//this will return response as json
    $attributes= \Yii::$app->request->post();
    date_default_timezone_set('Asia/Kolkata');
    $date= date('d-m-Y');
    $Booking=Booking::find()->select(['booking_id'])->where(['date_of_ride' => $date])->all();
    echo count($Booking);
    if($Booking>=0){
        $booking_today=count($Booking);
        return array('status'=> 'success', 'message'=>'Records found','total_booking'=>$booking_today);
    }
    else{
        return array('status'=> 'failed','data'=>'some error occured');
    }

}
    //Cancel Booking
    public function actionCancelbooking(){
        \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;//this will return response as json
        $attributes= \Yii::$app->request->post();
        $Booking = Booking::find()->where(['booking_id' => $attributes['booking_id'] ])->one();
        if($Booking){
            $Booking->book_status ='canceled';
            $Booking->save();
            return array('status'=> 'success','message'=>'Booking Cancelled');
            }
            else{
                return array('status'=>'failed','message'=>'invalid booking id');
            }


    }

    

    //Delete Booking Boat
    public function actionDeletebooking(){
        \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;//this will return response as json
        $attributes= \Yii::$app->request->post();
        $Booking = Booking::find()->where(['booking_id' => $attributes['booking_id'] ])->one();
        if($Booking)
        {
            $Booking->delete();
            return array('status'=>'success', 'data'=>'Record Deleted');
        }
        else
        {
            return array('status'=>'failed','data'=>'no records found');
        }


    }



    //View All Bookings
    public function actionViewbooking(){
        \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;//this will return response as json
        $attributes= \Yii::$app->request->post();
        $Booking= Booking::find()->where(['user_id' => $attributes['user_id'] ])->all();

        if(count($Booking)>0){
            return array('status'=> 'success', 'data'=>$Booking);
        }
        else
        {
            return array('status'=>'failed','data'=>'no records found');
        }
    }

    
    //Review Api
    public function actionReview(){
        \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;//this will return response as json
        $Review= new Review();
        $Review->scenario= Review::SCENARIO_CREATE;
        $Review->attributes= \Yii::$app->request->post();
        if($Review->validate()){
            $Review->save();
            return array('status'=>'success', 'message'=>'Review given','data'=>$Review);
        }
        else{
            return array('status'=>'failed','data'=>$Review->getErrors());
        }
     }
	  //View Reviews
     public function actionViewreviews(){
        \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;//this will return response as json
        $Review= Review::find()->all();

        if(count($Review)>0){
            return array('status'=> 'success', 'data'=>$Review);
        }
        else
        {
            return array('status'=>'failed','data'=>'no records found');
        }
    }


     //View Reviews
//      public function actionViewreviews(){
//         \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;//this will return response as json
//        $Review= Review::find()->all();
//       $Review= new Review();
//       $Review=Review::find()->select(['review_id','rating','remark','user_name'])->join('INNER JOIN','user_details','user_details.user_id=review.user_id')->all();
//        $Review=Review::find()->with('user')->all();
//        $reviewid_=[];
//       foreach($Review as $reviewid=>$record) {
//             $Ruser_id=$record['user_id'];
//              echo $Ruser_id;
//               $user=UserDetails::find()->select(['user_name'])->where(['user_id'=>$Ruser_id])->all();
//             echo (var_dump($user));
//            echo $user[0]['user_name'];
//             $usename_=[];
//              foreach($user as $userid=>$username)
//              {
//                  echo $username['user_name'];
//                $username=$user['user_name'];
//                 echo $usenam;
//                  $datause=array('user'=> $usenam);
//                array_push($usename_,$datause);
              
//             }  
//             $data=array('user_name'=>$usename_);
//             $data=array('reviewid'=>$reviewid,'user_name'=>$usename);
//            return array('data'=> $data); 
            
//             return array('data'=> $data);
//          }
        
    

// }

    //Delete Reviews
    // public function actionDeletereview(){
    //     \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;//this will return response as json
    //     $attributes= \Yii::$app->request->post();
    //     $Review = Review::find()->where(['review_id' => $attributes['review_id'] ])->one();
    //     if($Review)
    //     {
    //         $Review->delete();
    //         return array('status'=>'success', 'data'=>'Record Deleted');
    //     }
    //     else
    //     {
    //         return array('status'=>'failed','data'=>'no records found');
    //     }


    // }

    public function actionBanners(){
        \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;//this will return response as json
        //$user->attributes= \Yii::$app->request->get();

        $Banners= Banners::find()->all();

        if(count($Banners)>0){
            return array('status'=> 'success','message'=>'record found','data'=>$Banners);
        }
        else
        {
            return array('status'=>'failed','message'=>'no records found');
        }


    }

}
