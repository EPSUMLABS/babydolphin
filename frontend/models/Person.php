<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "person".
 *
 * @property int $person_id
 * @property int $booking_id
 * @property string $person_name
 * @property string $user_group
 * @property string $added_on
 * @property string $updated_on
 * @property string $is_delete
 *
 * @property Booking $booking
 */
class Person extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'person';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['person_id', 'booking_id', 'person_name', 'user_group', 'is_delete'], 'required'],
            [['person_id', 'booking_id'], 'integer'],
            [['added_on', 'updated_on'], 'safe'],
            [['person_name', 'user_group'], 'string', 'max' => 100],
            [['is_delete'], 'string', 'max' => 10],
            [['booking_id'], 'exist', 'skipOnError' => true, 'targetClass' => Booking::className(), 'targetAttribute' => ['booking_id' => 'booking_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'person_id' => 'Person ID',
            'booking_id' => 'Booking ID',
            'person_name' => 'Person Name',
            'user_group' => 'User Group',
            'added_on' => 'Added On',
            'updated_on' => 'Updated On',
            'is_delete' => 'Is Delete',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBooking()
    {
        return $this->hasOne(Booking::className(), ['booking_id' => 'booking_id']);
    }
}
