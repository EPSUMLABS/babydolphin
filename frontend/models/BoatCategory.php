<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "boat_category".
 *
 * @property int $boat_id
 * @property string $category_name
 * @property string $minimum_fare
 * @property string $capacity
 * @property string $min_fare_applicable
 * @property string $cost_add_on_person
 * @property string $added_on
 * @property string $updated_on
 * @property string $is_delete
 * @property string $number_of_boats
 *
 * @property Booking[] $bookings
 */
class BoatCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'boat_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_name', 'minimum_fare', 'capacity', 'min_fare_applicable', 'cost_add_on_person', 'is_delete', 'number_of_boats'], 'required'],
            [['added_on', 'updated_on'], 'safe'],
            [['category_name', 'minimum_fare', 'cost_add_on_person'], 'string', 'max' => 100],
            [['capacity', 'min_fare_applicable', 'number_of_boats'], 'string', 'max' => 50],
            [['is_delete'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'boat_id' => 'Boat ID',
            'category_name' => 'Category Name',
            'minimum_fare' => 'Minimum Fare',
            'capacity' => 'Capacity',
            'min_fare_applicable' => 'Min Fare Applicable',
            'cost_add_on_person' => 'Cost Add On Person',
            'added_on' => 'Added On',
            'updated_on' => 'Updated On',
            'is_delete' => 'Is Delete',
            'number_of_boats' => 'Number Of Boats',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookings()
    {
        return $this->hasMany(Booking::className(), ['boat_id' => 'boat_id']);
    }
}
