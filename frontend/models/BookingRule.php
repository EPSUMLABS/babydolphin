<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "booking_rule".
 *
 * @property int $id
 * @property string $allowed_upto
 * @property string $is_update
 * @property string $is_delete
 */
class BookingRule extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'booking_rule';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['allowed_upto', 'is_delete'], 'required'],
            [['is_update'], 'safe'],
            [['allowed_upto', 'is_delete'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'allowed_upto' => 'Allowed Upto',
            'is_update' => 'Is Update',
            'is_delete' => 'Is Delete',
        ];
    }
}
