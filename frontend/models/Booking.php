<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "booking".
 *
 * @property int $booking_id
 * @property int $boat_id
 * @property int $user_id
 * @property string $number_of_person
 * @property string $total_billing_amount
 * @property string $mode_of_pay
 * @property string $pay_status
 * @property string $date_of_booking
 * @property string $book_status
 * @property string $updated_on
 * @property string $is_delete
 * @property string $offer_applied
 * @property string $book_via
 * @property string $date_of_ride
 *
 * @property BoatCategory $boat
 * @property User $user
 * @property Person[] $people
 */
class Booking extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'booking';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['boat_id', 'user_id', 'number_of_person', 'total_billing_amount', 'mode_of_pay', 'pay_status', 'book_status', 'is_delete', 'offer_applied', 'book_via', 'date_of_ride'], 'required'],
            [['boat_id', 'user_id'], 'integer'],
            [['date_of_booking', 'updated_on'], 'safe'],
            [['number_of_person', 'total_billing_amount'], 'string', 'max' => 50],
            [['mode_of_pay', 'pay_status', 'book_status', 'is_delete', 'offer_applied', 'book_via', 'date_of_ride'], 'string', 'max' => 100],
            [['boat_id'], 'exist', 'skipOnError' => true, 'targetClass' => BoatCategory::className(), 'targetAttribute' => ['boat_id' => 'boat_id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'booking_id' => 'Booking ID',
            'boat_id' => 'Boat ID',
            'user_id' => 'User ID',
            'number_of_person' => 'Number Of Person',
            'total_billing_amount' => 'Total Billing Amount',
            'mode_of_pay' => 'Mode Of Pay',
            'pay_status' => 'Pay Status',
            'date_of_booking' => 'Date Of Booking',
            'book_status' => 'Book Status',
            'updated_on' => 'Updated On',
            'is_delete' => 'Is Delete',
            'offer_applied' => 'Offer Applied',
            'book_via' => 'Book Via',
            'date_of_ride' => 'Date Of Ride',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoat()
    {
        return $this->hasOne(BoatCategory::className(), ['boat_id' => 'boat_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeople()
    {
        return $this->hasMany(Person::className(), ['booking_id' => 'booking_id']);
    }
}
