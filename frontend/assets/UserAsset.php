<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class UserAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        // 'css/OwlCarousel2-2.2.1/owl.carousel.css',
        // 'css/OwlCarousel2-2.2.1/owl.theme.default.css',
        // 'css/OwlCarousel2-2.2.1/animate.css',
        'css/main_styles.css',
        //'css/responsive.css',
        //'css/bootstrap4/bootstrap.min.css',
        //'js/font-awesome-4.7.0/css/font-awesome.min.css',
        'css/usercss/style.default.css',
        'js/userjs/font-awesome/css/font-awesome.min.css',
        //'css/usercss/bootstrap/css/bootstrap.min.css'
    ];
    public $js = [
      //'js/jquery-3.2.1.min.js',
     // 'js/OwlCarousel2-2.2.1/owl.carousel.js',
      //'js/easing/easing.js',
      //'css/bootstrap4/popper.js',
      //'css/bootstrap4/bootstrap.min.js',
    ];
    public $depends = [
       // 'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
