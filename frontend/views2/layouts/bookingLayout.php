<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\UserAsset;
use common\widgets\Alert;

UserAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
	<script type="text/javascript">
   function myFunction(){
        $.ajax({
			url: 'http://babydolphin.epsumlabs.com/backend/web/api/app/viewbooking',
			type: 'POST',
			data: {"user_id":8},
			dataType: 'json'
		})
		.done(function(data){
		console.log(data)
       
        })
       
       
     }
		</script>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="super_container">
	
	<!-- Header -->

	<header class="header">

		<!-- Top Bar -->

		<div class="top_bar">
			<div class="container">
				<div class="row">
					<div class="col d-flex flex-row">
						<div class="phone">+45 345 3324 56789</div>
					
						<div class="user_box ml-auto">
							<div class="user_box_login user_box_link"><a data-toggle="modal" data-target="#loginmodal" style="color:white">login</a></div>
							<div class="user_box_register user_box_link"><a data-toggle="modal" data-target="#signupmodal" style="color:white">register</a></div>
						</div>
					</div>
				</div>
			</div>		
		</div>


</header>
    <div class="container">
        
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
