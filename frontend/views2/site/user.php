<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'User';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-user">
<div id="all">
      <div id="content">
        <div class="container"  style="padding-top:50px">
          <div class="row">
            <div class="col-lg-12">
              <!-- breadcrumb-->
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li aria-current="page" class="breadcrumb-item active">My account</li>
                </ol>
              </nav>
            </div>
            <div class="col-lg-3">
              <!--
              *** CUSTOMER MENU ***
              _________________________________________________________
              -->
              <div class="card sidebar-menu" >
                <div class="card-header">
                  <h3 class="h4 card-title">Customer section</h3>
                </div>
                <div class="card-body" style="padding:10px">
                <span style="display:flex;padding-left:20px;background-color:#186bbd" class="nav-link active"><i class="fa fa-list" style="padding-top:4px;padding-right:9px;color:white;"></i><?= Html::a('My Account', ['/site/user'],['style' => 'color:white;text-decoration:none']) ?></span>
                  <span style="display:flex;padding-left:20px;padding-top:20px"><i class="fa fa-heart" style="padding-top:4px;padding-right:9px;color:#555555;"></i><?= Html::a('My Bookings', ['/site/booking'],['style' => 'color:#555555;text-decoration:none']) ?></span>
                  <span style="display:flex;padding-left:20px;margin-top:20px;"><i class="fa fa-user" style="padding-top:4px;padding-right:9px;color:#555555;"></i><?= Html::a('View Reviews', ['/site/review'],['style' => 'color:#555555;text-decoration:none']) ?></span>
                  <span style="display:flex;padding-left:20px;padding-top:20px"><i class="fa fa-edit" style="padding-top:4px;padding-right:9px;color:#555555;"></i><?= Html::a('Submit Review', ['/site/submitreview'],['style' => 'color:#555555;;text-decoration:none']) ?></span>
                  
                </div>
              </div>
              <!-- /.col-lg-3-->
              <!-- *** CUSTOMER MENU END ***-->
            </div>
            <div class="col-lg-9">
              <div class="box">
              <h1>My account</h1>
                <h3 class="mt-2">Personal details</h3>
                <form>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="firstname">Username</label>
                        <input id="firstname" type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="company">EmailID</label>
                        <input id="company" type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="street">Contact Number</label>
                        <input id="street" type="text" class="form-control">
                      </div>
                    </div>
                  </div>
                  <!-- /.row-->
                  
                    <div class="col-md-12 text-center">
                      <button type="submit" class="btn" style="background-color:#0295ad;color:white"><i class="fa fa-save"></i> Save changes</button>
                    </div>
					 </form>
					
                <h3 class="mt-5">Change password</h3>
                <form>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="password_old">Old password</label>
                        <input id="password_old" type="password" class="form-control">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="password_1">New password</label>
                        <input id="password_1" type="password" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="password_2">Retype new password</label>
                        <input id="password_2" type="password" class="form-control">
                      </div>
                    </div>
                  </div>
                  <!-- /.row-->
                  <div class="col-md-12 text-center">
                    <button type="submit" class="btn" style="background-color:#0295ad;color:white"><i class="fa fa-save"></i> Save new password</button>
                  </div>
                </form>
                  
               
				</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

   
</div>
