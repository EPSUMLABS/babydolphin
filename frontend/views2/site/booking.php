<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Bookings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-booking">
<div id="all">
      <div id="content">
        <div class="container" style="padding-top:50px">
          <div class="row">
            <div class="col-lg-12">
              <!-- breadcrumb-->
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li aria-current="page" class="breadcrumb-item active">My Bookings</li>
                </ol>
              </nav>
            </div>
            <div class="col-lg-3">
              <!--
              *** CUSTOMER MENU ***
              _________________________________________________________
              -->
              <div class="card sidebar-menu" >
                <div class="card-header">
                  <h3 class="h4 card-title">Customer section</h3>
                </div>
                <div class="card-body" style="padding:10px">
                <span style="display:flex;padding-left:20px"><i class="fa fa-list" style="padding-top:4px;padding-right:9px;color:#555555;"></i><?= Html::a('My Account', ['/site/user'],['style' => 'color:#555555;text-decoration:none']) ?></span>
                  <span style="display:flex;padding-left:20px;margin-top:20px;background-color:#186bbd"  class="nav-link active"><i class="fa fa-heart" style="padding-top:4px;padding-right:9px;color:white;"></i><?= Html::a('My Bookings', ['/site/booking'],['style' => 'color:white;text-decoration:none']) ?></span>
                  <span style="display:flex;padding-left:20px;padding-top:20px"><i class="fa fa-user" style="padding-top:4px;padding-right:9px;color:#555555;"></i><?= Html::a('View Reviews', ['/site/review'],['style' => 'color:#555555;text-decoration:none']) ?></span>
                  <span style="display:flex;padding-left:20px;padding-top:20px"><i class="fa fa-edit" style="padding-top:4px;padding-right:9px;color:#555555;"></i><?= Html::a('Submit Review', ['/site/submitreview'],['style' => 'color:#555555;;text-decoration:none']) ?></span>
                </div>
              </div>
              <!-- /.col-lg-3-->
              <!-- *** CUSTOMER MENU END ***-->
            </div>
            <div id="customer-orders" class="col-lg-9">
              <div class="box">
                <h1>My Bookings</h1>
                <p class="lead">Your Bookings on one place.</p>
                
                <hr>
                <div class="table-responsive">
                  <table class="table table-hover">
                    <thead>
                      <tr>
                        <th>Booking ID</th>
                        <th>Number Of Persons</th>
                        <th>Total Amount</th>
                        <th>Date of Booking</th>
                        <th>Date Of Ride</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th># 1735</th>
                        <td>22/06/2013</td>
                        <td>$ 150.00</td>
                        <td><span class="badge badge-info">Being prepared</span></td>
                        <td><a href="customer-order.html" class="btn btn-primary btn-sm">View</a></td>
                      </tr>
                     
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

   
</div>
