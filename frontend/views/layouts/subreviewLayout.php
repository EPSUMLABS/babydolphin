<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\UserAsset;
use common\widgets\Alert;

UserAsset::register($this);
$session = Yii::$app->session;
$user_id = $session->get('user_id');
echo "<script>console.log($user_id)</script>";
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
	<script type="text/javascript">
    function submitreview(){
		var rating=$('input[name=rating]').val(); 
		var remark=$('textarea[name=remark]').val(); 
		console.log(remark)
        if(rating=="" && remark==""){
            window.alert("please fill the required field")
        }
        else{
        var formData = {
			'user_id':<?php echo $user_id?>,
            'rating':rating,
            'remark':remark,
        };
		console.log(formData);
        $.ajax({
            url: 'http://babydolphin.epsumlabs.com/backend/web/api/app/review',
            type: 'POST',
            data:formData,
			dataType: 'json'
		})
        .done(function(data){
		console.log(data)
        var available=data.status;
        console.log(available)
        if(available=="success"){
        window.alert("Your review added")
        }
        else{
            window.alert("Try again")
        }
        // bookfunction(available);
      
        })
			
        }
       
        

    }
    </script>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="super_container">
	
	<!-- Header -->

	<header class="header">

		<!-- Top Bar -->

		<div class="top_bar">
			<div class="container">
				<div class="row">
					<div class="col d-flex flex-row">
						<div class="phone">+45 345 3324 56789</div>
					
						<div class="user_box ml-auto">
                        <div class="user_box_login user_box_link"><a style="color:white">logout</a></div>
                            <?= Html::a('MY ACCOUNT', ['/site/user'],['style' => 'color:white;text-decoration:none;font-weight:700px;font-size:10px']) ?>
						
                        </div>
					</div>
				</div>
			</div>		
		</div>


</header>

    <div class="container">
        
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>



<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
