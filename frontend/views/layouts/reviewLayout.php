<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\UserAsset;
use common\widgets\Alert;

UserAsset::register($this);
$session = Yii::$app->session;
$user_id = $session->get('user_id');
echo "<script>console.log($user_id)</script>"
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
	<script type="text/javascript">
	 $.ajax({
			url: 'http://babydolphin.epsumlabs.com/backend/web/api/app/viewreviews',
			type: 'GET',
			dataType: 'json'
		})
		.done(function(data){
		console.log(data)
		if(data.status=="success")
        var value=data.data.length
        console.log(value)
		for(i=0;i<=value;i++){
		// rat=data.data[i]
		// console.log(rat)
		// document.getElementById("id").innerHTML=data.data[i].rating;
		// $('#motor_status').html('');
		
$('#id').append( '<hr><div class=""><span style="color:#555555;font-weight:bold;display:flex" id="username">User Name:&nbsp;&nbsp;&nbsp;<span style="color:#929191">'+data.data[i].user_id+'</span> <label style="padding-left:450px">Ratings star:&nbsp;&nbsp;&nbsp;</label><span style="color:#929191">'+data.data[i].rating+'</span></span></div>'
+'<div class=""><span style="color:#555555;font-weight:bold;display:flex">Date:&nbsp;&nbsp;&nbsp;<span style="color:#929191">'+data.data[i].updated_on+'</span></span></div>'+'<p><div class=""><span style="color:#555555;font-weight:bold">Review Details</span><br><p>'+data.data[i].remark+'</p></div></p></hr>')
		}
		
        // if(value==1){
        //     document.getElementById("statusval").innerHTML="Booking is allowed";
        // }
        // else{
        //     document.getElementById("statusval").innerHTML="Booking is Not allowed";
        // }
        })
	</script>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="super_container">
	<!-- Header -->
	<header class="header">
		<!-- Top Bar -->
		<div class="top_bar">
			<div class="container">
				<div class="row">
					<div class="col d-flex flex-row">
						<div class="phone">+45 345 3324 56789</div>
					
						<div class="user_box ml-auto">
						<div class="user_box_login user_box_link"><a style="color:white">logout</a></div>
                            <?= Html::a('MY ACCOUNT', ['/site/user'],['style' => 'color:white;text-decoration:none;font-weight:700px;font-size:10px']) ?>
						
						</div>
					</div>
				</div>
			</div>		
		</div>


</header>

    <div class="container">
        
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>



<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
