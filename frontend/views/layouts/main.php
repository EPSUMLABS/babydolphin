
<?php 

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
AppAsset::register($this);
$session = Yii::$app->session;
$user_id = $session->get('user_id');
echo "<script>console.log($user_id)</script>"

?>
<?php 
$this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
   
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Travelix Project">
<meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title>BabyDolphin</title>
    <script type="text/javascript">
    function checkavailability(){
        var dor=$('input[name=doravail]').val();
        if(dor==null){
            window.alert("please fill the data")
        }
        else{
        var formData = {
            'date_of_ride':dor,
            'category_name': $('#pack option:selected').val(),
        };
        }
       console.log(formData);
        $.ajax({
            url: 'http://babydolphin.epsumlabs.com/backend/web/api/app/availablebooking',
            type: 'POST',
            
            data:formData,
			dataType: 'json'
		})
        .done(function(data){
		console.log(data)
        var available=data.status;
        console.log(available)
        if(available=="success"){
        window.alert("Booking Available,"+data.availableboats+" "+"no. of seats are available")
        }
        else{
            window.alert("Booking is available upto 12:00pm")
        }
        // bookfunction(available);
      
        })
        

    }
    </script>
     <script type="text/javascript">
   function loginfunction(){
    var email=$('input[name=email]').val();   
    var password=$('input[name=password]').val();   
            var formData = {
            'user_email':email,
            'user_password':password
        };
       console.log(formData);
        $.ajax({
            url: 'http://babydolphin.epsumlabs.com/backend/web/api/app/userlogin',
            type: 'POST', 
            data:formData,
			dataType: 'json'
		})
        .done(function(data){ 
            console.log(data)
            if(data.status="success")
            {
                var userid=data.data.user_id;
                //console.log(userid); 
                window.alert("Welcome ") 
                // bookfunction(userid)
            
                    var formdata1={
                        'userid':userid,
                    }
                    $.ajax({
                    type: "POST",
                    url: "http://localhost/babydolphin/frontend/web/index.php/site/sample",
                    data:formdata1,
                    })
                    .done(function(data){
                        window.alert("value given") 
                    });
                    window.location.reload();
            }
    })
   }
   </script>
   <script type="text/javascript">
   function bookfunction(){
       //console.log(availval);
    
        $.ajax({
			url: 'http://babydolphin.epsumlabs.com/backend/web/api/app/viewboat',
			type: 'POST',
			dataType: 'json'
		})
		.done(function(data){
            //console.log(data)
            var capacity=data.data[0].capacity;
            var personcost=data.data[0].cost_add_on_person;
            var minfareperson=data.data[0].min_fare_applicable;
            var cost=data.data[0].minimum_fare;
            
        var nop=$('#nop').val();
        if(nop<=parseInt(capacity))
        {
            if(nop<=minfareperson)
            {
                var total_bill=cost;
            }
            else{
                var total_bill=parseInt(parseInt(cost)+parseInt(personcost*(nop-minfareperson)))
            }
            
            console.log(total_bill)
            var formData = {
             'user_id':<?php echo $user_id?>,   
            'boat_id':$('#boatcat option:selected').val(),
            'number_of_person': nop,
            'total_billing_amount':total_bill,
            'mode_of_pay':'online',
            'pay_status':'pending',
            'book_via':'web',
            'date_of_ride':$('#dorbook').val(),

        };
        console.log(formData);
        $.ajax({
			url: 'http://babydolphin.epsumlabs.com/backend/web/api/app/booking',
            type: 'POST',
            data:formData,
			dataType: 'json'
        })
        .done(function(data){
            console.log(data)
            if(data.status="success")
            {
             window.alert("Booking is done")
            }
            else{
            window.alert("Try again")  
            }
        })
        }
        else{
            window.alert("maximum boat capacity is "+capacity+".") 
        }
       
    })
    }
   </script>
   
    <script type="text/javascript">
   function registerfunction(){
    var name=$('input[name=name]').val(); 
    var email=$('input[name=useremail]').val(); 
    var contact=$('input[name=contact]').val();   
    var password=$('input[name=userpassword]').val();   
            var formData = {
            'user_name':name,
            'user_email':email,
            'user_contact':contact,
            'user_password':password
        };
       console.log(formData);
        $.ajax({
            url: 'http://babydolphin.epsumlabs.com/backend/web/api/app/adduser',
            type: 'POST', 
            data:formData,
			dataType: 'json'
		})
        .done(function(data){ 
            console.log(data)
            if(data.status="success")
            {
                window.alert("User added successfully")  
            }
            else{
                window.alert("Try again") 
            }
        })
   }
   </script>
       <script type="text/javascript">
   function forgotpassword(){
    var email_id=$('input[name=email_id]').val(); 
            var formData = {
            'user_email':email_id,
        };
       console.log(formData);
        $.ajax({
            url: 'http://babydolphin.epsumlabs.com/backend/web/api/app/userforgotpassword',
            type: 'POST', 
            data:formData,
			dataType: 'json'
		})
        .done(function(data){ 
            console.log(data)
            if(data.status="success")
            {
                window.alert("Check your inbox")  
            }
            else{
                window.alert("Try again") 
            }
        })
   }
   </script>
    <?php $this->head() ?>
    <style>
    .carousel-indicators{
        display:none;
    }
    </style>
</head>
<body>
<?php $this->beginBody() ?>
<div class="super_container">
	
	<!-- Header -->

	<header class="header">

		<!-- Top Bar -->

		<div class="top_bar">
			<div class="container">
				<div class="row">
					<div class="col d-flex flex-row">
						<div class="phone">+45 345 3324 56789</div>
						<div class="social">
							<!-- <ul class="social_list">
								<li class="social_list_item"><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
								<li class="social_list_item"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<li class="social_list_item"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								<li class="social_list_item"><a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
								<li class="social_list_item"><a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a></li>
								<li class="social_list_item"><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
							</ul> -->
						</div>
						<div class="user_box ml-auto">
                        <?php if ($user_id==null) {?>
							<div class="user_box_login user_box_link"><a data-toggle="modal" data-target="#loginmodal" style="color:white">login</a></div>
                            
							<div class="user_box_register user_box_link"><a data-toggle="modal" data-target="#signupmodal" style="color:white">register</a></div>
                           <? } else{?>
                          
                            <div class="user_box_login user_box_link"><a style="color:white">logout</a></div>
                            <?= Html::a('MY ACCOUNT', ['/site/user'],['style' => 'color:white;text-decoration:none;font-weight:600px']) ?>
                            <? } ?>
						</div>
					</div>
				</div>
			</div>		
		</div>

		<nav class="main_nav">
			<div class="container">
				<div class="row">
					<div class="col main_nav_col d-flex flex-row align-items-center justify-content-start">
						<div class="logo_container">
							<div class="logo"><a href="#"><img src="images/boat_logo1.png" style="width:170px" alt=""></a></div>
						</div>
						<div class="main_nav_container ml-auto">
							<ul class="main_nav_list">
								<li class="main_nav_item"><a href="#">home</a></li>
								<!--<li class="main_nav_item"><a href="about.html">about us</a></li>-->
								<li class="main_nav_item"><a href="#booknow">book now</a></li>
								<!--<li class="main_nav_item"><a href="blog.html">news</a></li>-->
								<!--<li class="main_nav_item"><a href="#contact">contact</a></li>-->
                                
							</ul>
						</div>
						

						

						<div class="hamburger">
							<i class="fa fa-bars trans_200"></i>
						</div>
					</div>
				</div>
			</div>	
		</nav>

	</header>

	

    

<div class="wrap">
    <?php
    // NavBar::begin([
    //     'brandLabel' => Yii::$app->name,
    //     'brandUrl' => Yii::$app->homeUrl,
    //     'options' => [
    //         'class' => 'navbar-inverse navbar-fixed-top',
    //     ],
    // ]);
    // $menuItems = [
    //     ['label' => 'Home', 'url' => ['/site/index']],
    //     ['label' => 'About', 'url' => ['/site/about']],
    //     ['label' => 'Contact', 'url' => ['/site/contact']],
    // ];
    // if (Yii::$app->user->isGuest) {
    //     $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
    //     $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    // } else {
    //     $menuItems[] = '<li>'
    //         . Html::beginForm(['/site/logout'], 'post')
    //         . Html::submitButton(
    //             'Logout (' . Yii::$app->user->identity->username . ')',
    //             ['class' => 'btn btn-link logout']
    //         )
    //         . Html::endForm()
    //         . '</li>';
    // }
    // echo Nav::widget([
    //     'options' => ['class' => 'navbar-nav navbar-right'],
    //     'items' => $menuItems,
    // ]);
    // NavBar::end();
    ?>

    <div>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer" style="padding-top:50px;padding-bottom:50px" >
    <div class="container">
        <p class="pull-left">BabyDolphin 2018</p>

        <p class="pull-right">Powered by Epsumlabs</p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>