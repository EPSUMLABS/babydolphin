<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\UserAsset;
use common\widgets\Alert;

UserAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
	<script type="text/javascript">
    function updateprofile(){
		var name=$('input[name=name]').val(); 
		var email=$('input[name=email]').val(); 
		var contact=$('input[name=contact]').val(); 
        if(name=="" && email=="" && contact==""){
            window.alert("please fill the required field")
        }
        else{
        var formData = {
			'user_id':8,
            'user_name':name,
            'user_email':email,
			'user_contact':contact,
        };
		console.log(formData);
        $.ajax({
            url: 'http://babydolphin.epsumlabs.com/backend/web/api/app/updateuser',
            type: 'POST',
            data:formData,
			dataType: 'json'
		})
        .done(function(data){
		console.log(data)
        var available=data.status;
        console.log(available)
        if(available=="success"){
        window.alert("User details updated")
        }
        else{
            window.alert("Try again")
        }
        // bookfunction(available);
      
        })
			
        }
       
        

    }
    </script>
	<script type="text/javascript">
    function changepassword(){
		var password=$('input[name=password]').val(); 
		var password1=$('input[name=password1]').val(); 
        if(password=="" && password1==""){
            window.alert("please fill the password")
        }
        else{
			if(password==password1){
        var formData = {
            'user_password':password,
            'user_id':8,
        };
		console.log(formData);
        $.ajax({
            url: 'http://babydolphin.epsumlabs.com/backend/web/api/app/userchangepassword',
            type: 'POST',
            
            data:formData,
			dataType: 'json'
		})
        .done(function(data){
		console.log(data)
        var available=data.status;
        console.log(available)
        if(available=="success"){
        window.alert("Password updated")
        }
        else{
            window.alert("Try again")
        }
        // bookfunction(available);
      
        })
			}
			else{
				window.alert("Password not matched")	
			}
        }
       
        

    }
    </script>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="super_container">
	
	<!-- Header -->

	<header class="header">

		<!-- Top Bar -->

		<div class="top_bar">
			<div class="container">
				<div class="row">
					<div class="col d-flex flex-row">
						<div class="phone">+45 345 3324 56789</div>
					
						<div class="user_box ml-auto">
                        <div class="user_box_login user_box_link"><a style="color:white">logout</a></div>
                            <?= Html::a('MY ACCOUNT', ['/site/user'],['style' => 'color:white;text-decoration:none;font-weight:700px;font-size:10px']) ?>
						
                        </div>
					</div>
				</div>
			</div>		
		</div>


</header>

    <div class="container">
        
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>



<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
