<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
<div class="menu trans_700">
		<div class="menu_content d-flex flex-column align-items-center justify-content-center text-center">
			<div class="menu_close_container"><div class="menu_close"></div></div>
			<img src="images/smallboat.png" alt="">
			<ul>
				<li class="menu_item"><a href="#">home</a></li>
				<!--<li class="menu_item"><a href="about.html">about us</a></li>-->
				<li class="menu_item"><a href="offers.html">Book Now</a></li>
				<!--<li class="menu_item"><a href="blog.html">news</a></li>-->
				<li class="menu_item"><a href="contact.html">contact</a></li>
				<li class="menu_item"><a href="profile.php">Profile</a></li>
			</ul>
		</div>
	</div>
  <div class="home" style="height:600px">
  <div id="myCarousel" class="carousel slide" data-ride="carousel" style="padding-top:10px">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <img src="images/banner1.png" alt="Los Angeles" style="width:100%;height:680px">
    </div>

    <div class="item">
      <img src="images/banner2.png" alt="Chicago" style="width:100%;height:680px">
    </div>

    <div class="item">
      <img src="images/banner3.png" alt="New York" style="width:100%;height:680px">
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>


	<!-- Search -->

	<div class="search" style="margin-top:95px;padding-left:255px" >
		

		<!-- Search Contents -->
		
		<div class="container fill_height">
			<div class="row fill_height">
				<div class="col fill_height">

					<!-- Search Tabs -->

					

					<!-- Search Panel -->
					<center><label style="font-size:25px;color:white;padding-right:250px">Check Availability</label></center>
					<div class="search_panel active" >
				
						<form action="#" id="search_form_1" class="search_panel_content d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-lg-between justify-content-start">
                        <div class="search_item">
								<div>Package</div>
								<select name="children" id="pack" name="pack" class="dropdown_item_select search_input">
									<option value="classic">Classic</option>
							
								</select>
							</div>
                            <div class="search_item" style='width:350px'>
								<div>Date Of Ride</div>
								<input type="text" id="doravail" name="doravail" class="check_in search_input" placeholder="DD-MM-YYYY" required>
							</div>
              <button type="button" class="button search_button"  onclick="checkavailability()">Check</button>
          	</form>
					</div>
			
					<!-- Search Panel -->
					<!-- Search Panel -->
				</div>
			</div>
		</div>		
	</div>
	</div>
	<!-- Intro -->
	
	<div class="intro" >
		<div class="container">
			<div class="row">
				<div class="col">
					<h2 class="intro_title text-center">We have the best tours</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-10 offset-lg-1">
					<div class="intro_text text-center">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam eu convallis tortor. Suspendisse potenti. In faucibus massa arcu, vitae cursus mi hendrerit nec. </p>
					</div>
				</div>
			</div>
			<div class="row intro_items">

				<!-- Intro Item -->

				<div class="col-lg-4 intro_col">
					<div class="intro_item">
						<div class="intro_item_overlay"></div>
						<!-- Image by https://unsplash.com/@dnevozhai -->
						<div class="intro_item_background" style="background-image:url(images/intro_1.jpg)"></div>
						<div class="intro_item_content d-flex flex-column align-items-center justify-content-center">
							
							
							
						</div>
					</div>
				</div>

				<!-- Intro Item -->

				<div class="col-lg-4 intro_col">
					<div class="intro_item">
						<div class="intro_item_overlay"></div>
						<!-- Image by https://unsplash.com/@hellolightbulb -->
						<div class="intro_item_background" style="background-image:url(images/intro_2.jpg)"></div>
						<div class="intro_item_content d-flex flex-column align-items-center justify-content-center">
							
							
							
						</div>
					</div>
				</div>

				<!-- Intro Item -->

				<div class="col-lg-4 intro_col">
					<div class="intro_item">
						<div class="intro_item_overlay"></div>
						<!-- Image by https://unsplash.com/@willianjusten -->
						<div class="intro_item_background" style="background-image:url(images/intro_3.jpg)"></div>
						<div class="intro_item_content d-flex flex-column align-items-center justify-content-center">
							
							
							
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	
	<div class="search" style='background:#258898;height:200px' id="booknow">
		
<center><label style="font-size:25px;color:white">Book Now</label></center>
		<!-- Search Contents -->
		
		<div class="container fill_height">
			<div class="row fill_height">
				<div class="col fill_height">
					<!-- Search Tabs -->
					<!-- Search Panel -->
					<div class="search_panel active">
						<form action="#" class="search_panel_content d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-lg-between justify-content-start">
							<div class="search_item">
								<div>Package</div>
								<select name="boatcat" id="boatcat" class="dropdown_item_select search_input">
									<option value="1">Classic</option>
							
								</select>
							</div>
							<div class="search_item">
								<div>Date Of Ride</div>
								<input type="text" id="dorbook" class="check_in search_input" placeholder="DD-MM-YYYY">
							</div>
							<!--<div class="search_item">
								<div>check out</div>
								<input type="text" class="check_out search_input" placeholder="YYYY-MM-DD">
							</div>-->
							<div class="search_item">
								<div>No. Of Persons</div>
								<input type="number" id="nop" class="check_in search_input" >
							</div>
							
							<!--<div>Total Amount</div>-->
								<input type="hidden" id="total_amt" name="total_amt" class="check_in search_input" >
						
							
							<input type="hidden" id="bookvia" value="web">
							<button type="button" class="button search_button" onclick="bookfunction()">Book Now</button>
						</form>
					</div>

					<!-- Search Panel -->

					
					<!-- Search Panel -->
				
					
				</div>
			</div>
		</div>		
	</div>

	<!-- CTA -->

	<!-- <div class="cta" id="contact"> -->
		<!-- Image by https://unsplash.com/@thanni -->
		<!-- <div class="cta_background" style="background-image:url(images/cta.jpg)"></div>
		<div class="container">
		<div class="card" style=">
		<div class="header">
		</div>
		<div class="body">
		<div class="container">
  <h2>Vertical (basic) form</h2>
  <form action="/action_page.php">
    <div class="form-group">
      <label for="email">Email:</label>
      <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
    </div>
    <div class="form-group">
      <label for="pwd">Password:</label>
      <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pwd">
    </div>
    <div class="checkbox">
      <label><input type="checkbox" name="remember"> Remember me</label>
    </div>
    <button type="submit" class="btn btn-default">Submit</button>
  </form>
</div>
		</div>
		</div>
		</div>
		</div>
		</div> -->
		<div id="loginmodal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
			<p><label style="font-size:24px;padding-left:165px">LOGIN HERE</label></p>
			<button type="button" class="close" data-dismiss="modal">&times;</button>
		
       
      </div>
      <div class="modal-body" style="height:200px">
			<form>
		<div class="form-group" >
		<input type="text" style="height:40px;font-size:16px" class="form-control" id="email" name="email" placeholder="Enter Email Address" required autocomplete />
		</div>
		<div class="form-group">
		<input type="password" style="height:40px;font-size:16px" class="form-control" id="password" name="password" placeholder="Enter your password" required autocomplete />
		</div>
		<div class="form-group">
		<button type="button" style="height:40px;font-size:16px;background-color:#258898;color:white" class="form-control btn"  onclick="loginfunction()">LOGIN</button>
		</div>
		</form>
      </div>
      <div class="modal-footer">
			<a style="font-size:18px;padding-right:250px;color:#717171" data-toggle="modal" data-target="#forgotpassmodal">Forgot Password</a>
        <button type="button" style="height:35px;font-size:16px" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<div id="forgotpassmodal" class="modal fade" role="dialog">
<div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
			<p><label style="font-size:24px;padding-left:165px">Forgot Password</label></p>
			<button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" style="height:200px">
			<form>
		<div class="form-group" >
		<input type="text" style="height:40px;font-size:16px" class="form-control" id="email_id" name="email_id" placeholder="Enter Email Address" required autocomplete />
		</div>
		<div class="form-group">
		<button type="button" style="height:40px;font-size:16px;background-color:#258898;color:white" class="form-control btn"   onClick="forgotpassword()">SUBMIT</button>
		</div>
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" style="height:35px;font-size:16px" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>
<div id="signupmodal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
			<p><label style="font-size:24px;padding-left:160px">USER SIGNUP</label></p>
			<button type="button" class="close" data-dismiss="modal">&times;</button>
		
       
      </div>
      <div class="modal-body" style="height:300px">
			<form>
		<div class="form-group" >
		<input type="text" style="height:40px;font-size:16px" class="form-control" name="name" placeholder="Enter Your Name" required autocomplete />
		</div>
		<div class="form-group" >
		<input type="text" style="height:40px;font-size:16px" class="form-control" name="useremail" placeholder="Enter Your Email Address" required autocomplete />
		</div>
		<div class="form-group" >
		<input type="text" style="height:40px;font-size:16px" class="form-control" name="contact" placeholder="Enter Your Contact No" required autocomplete />
		</div>
		<div class="form-group">
		<input type="password" style="height:40px;font-size:16px" class="form-control" name="userpassword" placeholder="Enter Your password" required autocomplete/>
		</div>
		<div class="form-group">
		<button type="button" style="height:40px;font-size:16px;background-color:#258898;color:white" class="form-control btn" onClick="registerfunction()">SIGNUP</button>
		</div>
		</form>
      </div>
      <div class="modal-footer">
			
        <button type="button" style="height:35px;font-size:16px" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>

</div>