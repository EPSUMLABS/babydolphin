<?php

/* @var $this yii\web\View */

$this->title = 'profile';
?>
<div class="site-index">
<div class="menu trans_700">
		<div class="menu_content d-flex flex-column align-items-center justify-content-center text-center">
			<div class="menu_close_container"><div class="menu_close"></div></div>
			<img src="images/smallboat.png" alt="">
			<ul>
				<li class="menu_item"><a href="#">home</a></li>
				<!--<li class="menu_item"><a href="about.html">about us</a></li>-->
				<li class="menu_item"><a href="offers.html">Book Now</a></li>
				<!--<li class="menu_item"><a href="blog.html">news</a></li>-->
				<li class="menu_item"><a href="contact.html">contact</a></li>
			</ul>
		</div>
	</div>
  <div class="home" style="height:600px">
  <div id="myCarousel" class="carousel slide" data-ride="carousel" style="padding-top:10px">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <img src="images/banner1.png" alt="Los Angeles" style="width:100%;height:680px">
    </div>

    <div class="item">
      <img src="images/banner2.png" alt="Chicago" style="width:100%;height:680px">
    </div>

    <div class="item">
      <img src="images/banner3.png" alt="New York" style="width:100%;height:680px">
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>


	